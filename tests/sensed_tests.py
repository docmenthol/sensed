import os
import toml
import threading
import socketserver

from dotmap import DotMap
from sensed.SensedServer import SensedServer
from sensed.SensedClient import SensedClient
from sensed.modules import datetime
from sensed.modules import netifaces
from sensed.modules import hat
from sensed.modules import camera


def test_test_mode() -> None:
    '''
    Tests server and client by starting the server in a thread, then
    requesting meta and sensor data. Asserts that this data is in the
    correct format and returns None on success.
    '''
    print('Finding default configs...')
    if os.path.isdir('docs'):
        server_config_file = 'docs/sensed-config.sample.toml'
        client_config_file = 'docs/senselog-config.sample.toml'
    elif os.path.isdir('../docs'):
        server_config_file = '../docs/sensed-config.sample.toml'
        client_config_file = '../docs/senselog-config.sample.toml'

    print('Running sensed test mode test...')

    with open(server_config_file) as f:
        server_config = DotMap(toml.load(f))
    dt_sensor = datetime.Sensor(server_config)
    ni_sensor = netifaces.Sensor(server_config)
    cam_sensor = camera.Sensor(server_config)
    hat_sensor = hat.Sensor(server_config)
    server_config.sensors = DotMap({'datetime': dt_sensor,
                                    'netifaces': ni_sensor,
                                    'camera': cam_sensor,
                                    'hat': hat_sensor})
    server = socketserver.UDPServer((server_config.sensed.host,
                                    server_config.sensed.port),
                                    SensedServer)
    server.sensors = server_config.sensors
    server.config = server_config

    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.daemon = True
    server_thread.start()

    with open(client_config_file) as f:
        client_config = DotMap(toml.load(f))
    client = SensedClient(client_config)

    meta = client.get_all_meta()
    sensors = client.get_all_sensors()

    assert isinstance(meta, list)
    assert isinstance(meta[0], dict)
    assert isinstance(sensors, list)
    assert isinstance(sensors[0], dict)

    print('Packet test passed. Shutting down...')

    server.shutdown()

    print('Test complete.')


def test_deployment_mode() -> None:
    '''
    Tests server and client by starting the server in a thread, then
    requesting meta and sensor data. Asserts that this data is in the
    correct format and returns None on success.
    '''
    print('Finding default configs...')
    if os.path.isdir('docs'):
        server_config_file = 'docs/sensed-config.sample.toml'
        client_config_file = 'docs/senselog-config.sample.toml'
    elif os.path.isdir('../docs'):
        server_config_file = '../docs/sensed-config.sample.toml'
        client_config_file = '../docs/senselog-config.sample.toml'

    print('Running sensed deployment mode test...')

    with open(server_config_file) as f:
        server_config = DotMap(toml.load(f))
    server_config.test = False
    dt_sensor = datetime.Sensor(server_config)
    ni_sensor = netifaces.Sensor(server_config)
    server_config.sensors = DotMap({'datetime': dt_sensor,
                                    'netifaces': ni_sensor})
    server = socketserver.UDPServer((server_config.sensed.host,
                                    server_config.sensed.port),
                                    SensedServer)
    server.sensors = server_config.sensors
    server.config = server_config

    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.daemon = True
    server_thread.start()

    with open(client_config_file) as f:
        client_config = DotMap(toml.load(f))
    client = SensedClient(client_config)

    meta = client.get_all_meta()
    sensors = client.get_all_sensors()

    assert isinstance(meta, list)
    assert isinstance(meta[0], dict)
    assert isinstance(sensors, list)
    assert isinstance(sensors[0], dict)

    print('Packet test passed. Shutting down...')

    server.shutdown()

    print('Test complete.')


if __name__ == '__main__':
    test_test_mode()
    test_deployment_mode()