from typing import Tuple, List, Dict, Any

Host = Tuple[str, int]
HostList = List[Host]
MetaData = Dict[str, Any]
SensorData = Dict[str, Any]
SensorList = List[str]
