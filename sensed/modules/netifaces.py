import netifaces
from dotmap import DotMap

class NetIFaces(object):
    def __init__(self, config: DotMap) -> None:
        self.config = config['sensed-modules-netifaces']
        self.ifaces = [i for i in netifaces.interfaces() \
                       if i not in self.config.ignore]
        self.addrs = {k: netifaces.ifaddresses(k) for k in self.ifaces}

    def get_data(self) -> dict:
        return {'interfaces': self.ifaces, 'addresses': self.addrs}

    def test(self) -> dict:
        return self.get_data()


Sensor = NetIFaces
