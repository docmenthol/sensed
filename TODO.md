# TODO

* &#9745; Unify client and server libraries - Done
* &#9745; Add testing - Done
* &#9745; Error handling and recovery for broken modules - Done
* &#9745; Add a setting for custom module directories
* &#9744; Threaded server

* &#9744; Reactive/push version of server
* &#9744; Reactive/push version of client