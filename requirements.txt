click==6.6
pychalk==1.1.0
msgpack-python==0.4.8
toml==0.9.2
dotmap==1.2.0
netifaces==0.10.4
