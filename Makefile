init:
	pip3 install -r requirements.txt
	python setup.py install

tests:
	coverage run -p scripts/sensed -c docs/sensed-config.sample.toml --ci
	coverage run -p scripts/senselog -c docs/senselog-config.sample.toml --ci
	coverage combine
	coverage report